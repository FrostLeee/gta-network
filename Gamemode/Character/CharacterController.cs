﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using GTANetworkServer;
using GTANetworkShared;

using Gamemode.Server.Account;
using Gamemode.Server.Database.Context;

namespace Gamemode.Server.Characters
{
    public class CharacterController
    {
        public Character character = new Character();
        public AccountController AccountController { get; private set; }
        public string formatName { get; private set; }
        public string[] formatMember { get; private set; }
        public string[] formatCoord { get; private set; }
        public string[] formatDefaultModel { get; private set; }

        public static int userCharacterMax = 5;

        public CharacterController(AccountController account, Character data)
        {
            character = data;
            AccountController = account;

            formatName = character.name.Replace("_", " ");

            /**
             *  Преобразовываем координаты в нормальный вид
             *  
             *  formatCoord[0] - Coord.X
             *  formatCoord[1] - Coord.Y
             *  formatCoord[2] - Coord.Z
             *  formatCoord[3] - Rotation.Z
             */
            formatCoord = character.coord.Split(';');

            /**
             *  Преобразовываем модель игрока в нормальный вид
             *
             *  formatDefaultModel[0] - ModelHash
             *  formatDefaultModel[1] - ModelName
             */
            formatDefaultModel = character.defaultModel.Split(';');

            /**
             *  Преобразовываем организацию игрока в нормальный вид
             *
             *  formatMember[0] - ID организации
             *  formatMember[1] - Является ли игрок лидером органиции (0 - не является, 1 - заместитель, 2 - лидер организации)
             *  formatMember[2] - Ранг игрока
             */
            formatMember = String.IsNullOrEmpty(character.member) ? null : character.member.Split(';');


            switch (character.registrationStep)
            {
                case 0:
                    API.shared.sendChatMessageToPlayer(AccountController.Client, $"Добро пожаловать ~b~{formatName}");
                    break;

                default:
                    API.shared.sendChatMessageToPlayer(AccountController.Client, $"Добро пожаловать ~b~{formatName}~w~! Ваша последняя авторизация была ~b~{character.lastLoginDate}");
                    break;
            }

            character.lastLoginDate = DateTime.UtcNow;
            character.online = true;
            ContextFactory.Instance.SaveChanges();
        }

        public CharacterController(AccountController account, string name, int sex)
        {
            account.CharaterController = this;
            character.userid = account.account.id;
            character.name = name;
            character.defaultModel = String.Format("{0};{1}", PedHash.TrampBeac01AMM.GetHashCode(), "TrampBeac01AMM");
            character.registerDate = DateTime.UtcNow;

            ContextFactory.Instance.Character.Add(character);
            ContextFactory.Instance.SaveChanges();
        }

        public void save()
        {
            character.coord = String.Format("{0};{1};{2};{3}", 
                    AccountController.Client.position.X,
                    AccountController.Client.position.Y,
                    AccountController.Client.position.Z,
                    AccountController.Client.rotation.Z
                );

            character.defaultModel = String.Format("{0}",
                    AccountController.Client.model.GetHashCode()
                );
        }

        public static void CharacterMenu(AccountController AccountController)
        {
            var CharacterMenuEn = new List<string>();
            var Character = AccountController.account.character;

            if(Character == null || Character.Count == 0)
            {
                CharacterMenuEn.Add("Создать персонажа");
                API.shared.triggerClientEvent(AccountController.Client, "create_menu", "character_login", null, $"Персонажей (0 / {userCharacterMax})", true, CharacterMenuEn.ToArray());
            }
            else
            {
                foreach (var character in Character)
                {
                    CharacterMenuEn.Add(character.name.Replace("_", " "));
                }

                if(Character.Count <= userCharacterMax) CharacterMenuEn.Add("~g~Создать персонажа");

                API.shared.triggerClientEvent(AccountController.Client, "create_menu", "character_login", null, $"Персонажей ({Character.Count} / {userCharacterMax})", true, CharacterMenuEn.ToArray());
            }
        }

        public static void createCharacter(Client player)
        {
            API.shared.triggerClientEvent(player, "create_character");
        }

        public static void selectCharacter(Client player, int id)
        {
            AccountController account = player.getData("ACCOUNT");
            if (account == null) return;

            if(account.account.character.Count() == 0)
            {
                API.shared.sendChatMessageToPlayer(player, "У вас нет ниодного персонажа!");
            }
            else
            {
                int characterId = account.account.character.ToList()[id - 1].id;
                Character data = ContextFactory.Instance.Character.FirstOrDefault(x => x.id == characterId);
                CharacterController characterController = new CharacterController(account, data);
                characterController.LoginCharacter(account);
            }
        }

        public void LoginCharacter(AccountController accountController)
        {
            AccountController = accountController;
            accountController.CharaterController = this;

            SpawnManager.SpawnCharacter(this);

            accountController.Client.freeze(false);
            accountController.Client.transparency = 255;
            accountController.Client.nametag = formatName + " (" + accountController.playaerid + ")";
        }

        public static bool nameValidation(Client player, string name)
        {
            if (!name.Contains("_") || name.Count(x => x == '_') > 1)
            {
                API.shared.sendChatMessageToPlayer(player, "~r~Ошибка: ~w~У персонажа должна быть фамилия. Пожалуйста отделите имя и фамилию символом ~b~'_'~w~.");
                return false;
            }
            else if (Regex.IsMatch(name, @"^[a-zA-Z_]+$")) return true;

            API.shared.sendChatMessageToPlayer(player, "~r~Ошибка: ~w~Вы ввели недопустимое имя.");
            return false;
        }
    }
}
