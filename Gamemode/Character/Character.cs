﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Gamemode.Server.Data;

namespace Gamemode.Server.Characters
{
    [Table("character")]
    public class Character
    {
        public Character()
        {
        }

        [Key]
        public int id { get; set; }

        public int userid { get; set; }
        public InterfaceUser user { get; set;}

        [StringLength(32)]
        public string name { get; set; }
        public DateTime registerDate { get; set; }
        public DateTime lastLoginDate { get; set; }
        public int registrationStep { get; set; }
        public bool online { get; set; }

        [StringLength(256)]
        public string coord { get; set; }
        public string defaultModel { get; set; }
        public string member { get; set; }

        [DefaultValue(50)]
        public float money { get; set; }

        [DefaultValue(0)]
        public int level { get; set; }
        public int exp { get; set; }
        public float bank { get; set; }
        public int admin { get; set; }

        public int job { get; set; }

    }
}
