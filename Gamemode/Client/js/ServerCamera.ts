﻿/// <reference path="../../types-gtanetwork/index.d.ts" />

API.onServerEventTrigger.connect(function (event, args) {
    if (event == "loginCamera") {
        var FreeCam = API.createCamera(args[0], new Vector3(0, 0, 0));
        API.setActiveCamera(FreeCam);
        API.setPlayerInvincible(true);
        API.setHudVisible(false);
    }
});