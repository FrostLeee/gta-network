﻿var menuPool = null;
var draw = false;

API.onServerEventTrigger.connect(function (name, args) {
    if (name == "create_menu") {
        var callback    = args[0],
            setbanner   = args[1],
            title       = args[2],
            exit        = args[3],
            item        = args[4],

            menu        = null;

        menu = (setbanner == null) ? API.createMenu(title, 0, 0, 6) : API.createMenu(setbanner, title, 0, 0, 6);

        if (exit) menu.ResetKey(menuControl.Back);

        for (var i = 0; i < item.Count; i++) {
            menu.AddItem(API.createMenuItem(item[i], ""));
        }

        menu.RefreshIndex();

        menu.OnItemSelect.connect(function (sender, item, index) {
            API.triggerServerEvent("menu_handler_select_item", callback, index, item.Count);
            menu.Visible = false;
        });

        API.onUpdate.connect(function () {
            API.drawMenu(menu);
        });

        menu.Visible = true;

    }
});


API.onUpdate.connect(function() {

    if (menuPool != null) menuPool.ProcessMenus();
});