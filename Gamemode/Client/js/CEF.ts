﻿/// <reference path="../../types-gtanetwork/index.d.ts" />

var browser = null;
var Cam = null;
API.onServerEventTrigger.connect(function (event, params) {
    switch (event) {
        case "loginPlayer":
            var screen = API.getScreenResolution();
            browser = API.createCefBrowser(500, 500);
            API.setCefBrowserPosition(browser, (screen.Width / 2) - (500 / 2), (screen.Height / 2) - (500 / 2));
            API.loadPageCefBrowser(browser, "Client/pages/authorize.html");
            API.showCursor(true);
            API.setCanOpenChat(false);

            Cam = API.createCamera(params[0], new Vector3(0, 0, 0));
            API.setActiveCamera(Cam);
            API.setPlayerInvincible(true);
            API.setHudVisible(false);
            break;

        case "create_character":
            var screen = API.getScreenResolution();
            browser = API.createCefBrowser(500, 500);
            API.setCefBrowserPosition(browser, (screen.Width / 2) - (500 / 2), (screen.Height / 2) - (500 / 2));
            API.loadPageCefBrowser(browser, "Client/pages/createCharacter.html");
            API.showCursor(true);
            API.setCanOpenChat(false);

            Cam = API.createCamera(params[0], new Vector3(0, 0, 0));
            API.setActiveCamera(Cam);
            API.setPlayerInvincible(true);
            API.setHudVisible(false);
            break;

        case "destroyCef":
            API.showCursor(false);
            API.setCanOpenChat(true);
            API.destroyCefBrowser(browser);
            break;

        case "destroyCamera":
            API.setActiveCamera(null);
            API.setPlayerInvincible(false);
            API.setHudVisible(true);
            break;
    }
});

function loginResource(event, email, password) {
    API.triggerServerEvent(event, email, password);
}

function createCharacter(name, verify, sex) {
    if (name == verify) {
        API.triggerServerEvent("crater_character", name, sex);
    } else {
        API.sendChatMessage("~r~Ошибка: ~w~Не совпадают имена");
    }
}