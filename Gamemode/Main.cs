﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using GTANetworkServer;
using GTANetworkShared;

using Gamemode.Server.Database.Context;
using Gamemode.Server.Account;

namespace Gamemode.Server
{
    public class Main : Script
    {
        public Main()
        {
            API.onResourceStart += onResourceStart;
            API.onResourceStop += onResourceStop;
        }

        private void onResourceStart()
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            API.consoleOutput( API.getSetting<string>("server_name") + " запущен, время: " + DateTime.Now );
            Console.ResetColor();
        }
        
        private void onResourceStop()
        {
            List<Client> allPlayers = API.getAllPlayers();
            foreach (var user in allPlayers)
                ConnectionController.Logout(user);
        }


        [Command("veh", GreedyArg = true)]
        public void commandVeh(Client player, VehicleHash model)
        {
            Vector3 pos = API.getEntityPosition(player);
            API.createVehicle(model, pos, new Vector3(0, 0, 0), 0, 0);
        }

        [Command("save")]
        public void commandPos(Client player)
        {
            var pos = API.getEntityPosition(player);
            var rot = API.getEntityRotation(player);
            File.AppendAllText("savepos.txt", string.Format("Position - new Vector3({0}, {1}, {2}); Rotation - new Vector3({3}, {4}, {5});\n", pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z));
            API.sendNotificationToPlayer(player, string.Format("Position saved as: {0}, {1}, {2}", pos.X, pos.Y, pos.Z), true);
        }


        [Command("test")]
        public void commandTest(Client player)
        {
            List<Client> test = API.getAllPlayers();
            foreach(var a in test)
            {
                AccountController user = a.getData("ACCOUNT");
                if (user == null) return;

                user.account.online = false;
                ContextFactory.Instance.SaveChanges();
            } 
        }
    }
}
