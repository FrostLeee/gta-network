﻿using System;
using System.Linq;

using GTANetworkServer;
using GTANetworkShared;
using Net = BCrypt.Net;

using Gamemode.Server.Account;
using Gamemode.Server.Database.Context;
using Gamemode.Server.Characters;

namespace Gamemode.Server.Database
{
    class DatabaseManager : Script
    {
        public DatabaseManager()
        {
            API.onResourceStart += onResourceStart;
            API.onClientEventTrigger += onClientEventTrigger;
        }

        private void onResourceStart()
        {
            ContextFactory.SetConnectionParameters(
                API.getSetting<string>("database_server"),
                API.getSetting<string>("database_user"),
                API.getSetting<string>("database_password"),
                API.getSetting<string>("database_name")    
            );

            var count = ContextFactory.Instance.User.Count();
            API.consoleOutput("Игроков в базе данных: " + count);
        }

        private void onClientEventTrigger(Client player, string eventName, params object[] args)
        {
            switch (eventName)
            {
                case "authorize":
                    this.authorize(player, args[0].ToString(), args[1].ToString());
                    break;

                case "registration":
                    this.registration(player, args[0].ToString(), args[1].ToString());
                    break;

                case "crater_character":
                    this.craterCharacter(player, args[0].ToString(), (int) args[1]);
                    break;
            }
        }

        public static bool DoesAccountExits(string email)
        {
            return (ContextFactory.Instance.User.FirstOrDefault(u => u.email == email) == null) ? false : true;
        }

        private void registration(Client player, string email, string password)
        {
            if(!DoesAccountExits(email))
            {
                User account = new User
                {
                    socialClub = player.name,
                    email = email,
                    password = Net.BCrypt.HashPassword(password, Net.BCrypt.GenerateSalt(12)),
                    registerDate = DateTime.UtcNow,
                    lastLoginDate = DateTime.UtcNow,
                    registerIP = player.address,
                    lastIP = player.address,
                    online = true
                };

                ContextFactory.Instance.User.Add(account);
                ContextFactory.Instance.SaveChanges();

                new AccountController(account, player);

                /*player.freeze(false);
                player.transparency = 1000;
                player.position = new Vector3(-1039.092, -2733.447, 13.75664);*/
            }
        }

        private void authorize(Client player, string email, string password)
        {
            var user = ContextFactory.Instance.User.FirstOrDefault(u => u.email == email);
            if (user != null)
            {
                if (Net.BCrypt.Verify(password, user.password))
                {
                    new AccountController(user, player);

                    /*player.freeze(false);
                    player.transparency = 1000;
                    player.position = new Vector3(-1039.092, -2733.447, 13.75664);*/
                }
                else
                    API.sendChatMessageToPlayer(player, $"~r~ВНИМАНИЕ! ~w~ Введён не правильный логин или пароль!");
            }
            else
                API.sendChatMessageToPlayer(player, $"~r~ВНИМАНИЕ! ~w~ Введён не правильный логин или пароль!");
        }

        public static bool DoesCharacterExist(string name)
        {
            if (ContextFactory.Instance.Character.Where(x => x.name == name).FirstOrDefault() == null) return false;
            return true;
        }

        public static bool HasCharacterSlot(AccountController account)
        {
            if (account.account.character == null) return true;

            if (account.account.character.Count() > CharacterController.userCharacterMax)
            {
                API.shared.sendChatMessageToPlayer(account.Client, "~r~Ошибка: ~w~Превышен лимит персонажей!");
                return false;
            }
            return true;
        }

        private void craterCharacter(Client player, string name, int sex)
        {
            AccountController account = player.getData("ACCOUNT");
            if (account == null) return;

            if ( !CharacterController.nameValidation(player, name) ) return;
            if( !DoesCharacterExist(name) && HasCharacterSlot(account))
            {
                new CharacterController(account, name, sex);

                API.shared.sendChatMessageToPlayer(player, "~g~Server: ~w~Character created!");
                CharacterController.CharacterMenu(account);
            }
        }
    }
}
