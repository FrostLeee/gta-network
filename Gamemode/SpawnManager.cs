﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GTANetworkServer;
using GTANetworkShared;
using Gamemode.Server.Characters;
using Gamemode.Server.Database.Context;

namespace Gamemode.Server
{
    class SpawnManager : Script
    {
        private static readonly Vector3 _newPlayerPosition = new Vector3(-1034.794, -2727.422, 13.75663);
        private static readonly Vector3 _newPlayerRotation = new Vector3(0.0, 0.0, -34.4588);
        private static readonly int _newPlayerDimension = 0;

        public SpawnManager()
        {

        }

        public static void SpawnCharacter(CharacterController character)
        {
            API.shared.triggerClientEvent(character.AccountController.Client, "destroyCamera");

            Client target = character.AccountController.Client;

            API.shared.setPlayerSkin(character.AccountController.Client, (PedHash)Convert.ToInt32(character.formatDefaultModel[0]));

            API.shared.resetPlayerNametagColor(target);
            API.shared.removeAllPlayerWeapons(target);

            if (character.character.registrationStep == 0)
            {
                API.shared.setEntityPosition(target, _newPlayerPosition);
                API.shared.setEntityRotation(target, _newPlayerRotation);
                character.character.registrationStep = -1; // 'Tutorial Done'
                //character.character.ModelName = API.shared.getEntityModel(character.AccountController.Client).ToString();
            }
            else
            {
                API.shared.setEntityPosition(character.AccountController.Client, new Vector3(Convert.ToSingle(character.formatCoord[0]), Convert.ToSingle(character.formatCoord[1]), Convert.ToSingle(character.formatCoord[2])));
                API.shared.setEntityRotation(character.AccountController.Client, new Vector3(0.0f, 0.0f, Convert.ToSingle(character.formatCoord[3])));
                //character.Character.ModelName = API.shared.getEntityModel(character.AccountController.Client).ToString();

            }

            ContextFactory.Instance.SaveChanges();
        }


    }
}
