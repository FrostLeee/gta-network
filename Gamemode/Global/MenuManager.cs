﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GTANetworkServer;
using Gamemode.Server.Characters;

namespace Gamemode.Server.Menu
{
    public class MenuManager : Script
    {
        public MenuManager()
        {
            API.onClientEventTrigger += onClientEventTrigger;
        }

        private void onClientEventTrigger(Client player, string eventName, object[] args)
        {
            if( eventName == "menu_handler_select_item" )
            {
                string callback = (string) args[0];
                switch(callback)
                {
                    case "character_login":
                        if ((int)args[1] == (int)args[2] - 1)
                            CharacterController.createCharacter(player);
                        else
                            CharacterController.selectCharacter(player, (int)args[1] + 1);
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
