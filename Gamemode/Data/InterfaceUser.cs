﻿using System;
using System.Collections.Generic;

using Gamemode.Server.Characters;

namespace Gamemode.Server.Data
{
    public interface InterfaceUser
    {
        int id { get; set; }

        string socialClub { get; set; }

        string password { get; set; }

        string email { get; set; }

        string registerIP { get; set; }

        string lastIP { get; set; }

        DateTime registerDate { get; set; }

        DateTime lastLoginDate { get; set; }

        bool online { get; set; }

        ICollection<Character> character { get; set; }
    }
}
