﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Gamemode.Server.Data;
using Gamemode.Server.Characters;

namespace Gamemode.Server.Account
{
    [Table("users")]
    public class User : InterfaceUser
    {
        public User() { }

        [Key]
        public int id { get; set; }
        public string socialClub { get; set; }
        public string password { get; set; }

        [StringLength(24)]
        public string email { get; set; }

        [StringLength(16)]
        public string registerIP { get; set; }
        public string lastIP { get; set; }
        public DateTime registerDate { get; set; }
        public DateTime lastLoginDate { get; set; }

        public bool online { get; set; }

        public virtual ICollection<Character> character { get; set; }
    }
}
