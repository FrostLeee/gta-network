﻿using GTANetworkServer;
using GTANetworkShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gamemode.Server.Database.Context;
using Net = BCrypt.Net;
using Gamemode.Server.Account;

namespace Gamemode.Server
{
    public class ConnectionController : Script
    {
        public ConnectionController()
        {
            API.onPlayerConnected += onPlayerConnected;
            API.onPlayerDisconnected += onPlayerDisconnectedHandler;
            API.onPlayerFinishedDownload += onPlayerFinishedDownload;
            API.onChatMessage += onChatMessage;
        }

        private void onPlayerConnected(Client player)
        {
        }

        private void onPlayerDisconnectedHandler(Client player, string reason)
        {
            Logout(player);
        }

        private void onPlayerFinishedDownload(Client player)
        {
            API.setEntityData(player, "DOWNLOAD_FINISHED", true);
            WindowLogin(player);
        }

        private void onChatMessage(Client player, string message, CancelEventArgs e)
        {
            AccountController user = player.getData("ACCOUNT");
            if (user == null) return;
            API.sendChatMessageToAll($"{user.CharaterController.formatName}: {message}");
            e.Cancel = true;
            return;
        }

        public void WindowLogin(Client player)
        {
            API.triggerClientEvent(player, "loginPlayer", new Vector3(824.7343, 1036.479, 327.9344));

            player.freeze(true);
            player.transparency = 0;
        }

        public static void Logout(Client player)
        {
            AccountController user = player.getData("ACCOUNT");
            if (user == null) return;

            user.account.online = false;
            ContextFactory.Instance.SaveChanges();
        }
    }
}
