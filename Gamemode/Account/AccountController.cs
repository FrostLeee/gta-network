﻿using System;
using System.Linq;
using System.Text;
using Gamemode.Server.Database.Context;
using Gamemode.Server.Data;
using Gamemode.Server.Characters;

using GTANetworkServer;

namespace Gamemode.Server.Account
{
    public class AccountController
    {
        public string accountID;
        public InterfaceUser account;
        public int playaerid;

        public Client Client { get; set; }
        public CharacterController CharaterController;

        public AccountController(Account.User user, Client player)
        {
            account = user;
            Client = player;

            player.setData("ACCOUNT", this);

            account.lastLoginDate = DateTime.UtcNow;
            account.lastIP = player.address;
            account.online = true;

            CharacterController.CharacterMenu(this);

            API.shared.triggerClientEvent(player, "destroyCef");
            ContextFactory.Instance.SaveChanges();
        }
    }

}
